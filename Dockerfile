FROM golang:1.13.4-alpine as builder
# Install the build tools we need
RUN apk add --no-cache make curl git build-base gcc g++ libc-dev binutils-gold

# Select our working directory
RUN mkdir -p /go/src/github.com/aosapps/drone-sonar-plugin
WORKDIR /go/src/github.com/aosapps/drone-sonar-plugin 

# Clone the project to working directory
RUN git clone https://github.com/aosapps/drone-sonar-plugin.git .

#Build the files
RUN GOOS=linux GOARCH=arm64 CGO_ENABLED=0 go build -o drone-sonar

#Start with new Jre image
FROM openjdk:8-jre-alpine

ARG SONAR_VERSION=4.2.0.1873
ARG SONAR_SCANNER_CLI=sonar-scanner-cli-${SONAR_VERSION}
ARG SONAR_SCANNER=sonar-scanner-${SONAR_VERSION}

RUN apk add --no-cache --update nodejs curl
COPY --from=builder /go/src/github.com/aosapps/drone-sonar-plugin/drone-sonar /bin/
WORKDIR /bin

RUN curl https://binaries.sonarsource.com/Distribution/sonar-scanner-cli/${SONAR_SCANNER_CLI}.zip -so /bin/${SONAR_SCANNER_CLI}.zip
RUN unzip ${SONAR_SCANNER_CLI}.zip \
    && rm ${SONAR_SCANNER_CLI}.zip \
    && apk del curl

ENV PATH $PATH:/bin/${SONAR_SCANNER}/bin

